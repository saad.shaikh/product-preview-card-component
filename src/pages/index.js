import React, { Fragment } from "react"
import { Helmet } from "react-helmet";
import "../app.css"
import Favicon from "../images/favicon.jpg"
import IconCart from "../images/icon-cart.svg"


const IndexPage = () => {
  return (
    <Fragment>
      <Helmet htmlAttributes={{
        lang: 'en',
      }} >
        <title>Product preview card component</title>
        <link rel="icon" type="image/x-icon" href={Favicon}></link>
      </Helmet>

      <div className="card" role="main">
        <div className="card-image"></div>
        <div className="card-text">
          <span className="product-name">PERFUME</span>
          <h1 className="title">Gabrielle Essence Eau De Parfum</h1>
          <p className="desc">
            A floral, solar and voluptuous interpretation composed by Olivier Polge,
            Perfumer-Creator for the House of CHANEL.
          </p>
          <div className="price">
            <span className="discounted-price">$149.99</span>
            <s className="original-price">$169.99</s>
          </div>
          <button className="button">
            <img src={IconCart} alt="Icon Cart"></img>
            <span className="button-text">Add to Cart</span>
          </button>
        </div>
      </div>

      <footer class="attribution">
        Challenge by <a href="https://www.frontendmentor.io?ref=challenge" target="_blank" rel="noreferrer">Frontend Mentor</a>.
        Coded by <a href="https://saad-shaikh-portfolio.netlify.app/" target="_blank" rel="noreferrer">Saad Shaikh</a>.
      </footer>
    </Fragment >
  )
}

export default IndexPage
