# Title: Product preview card component

Tech stack: Gatsby

Deployed project: https://saad-shaikh-product-preview-card-component.netlify.app/

## Main tasks:
- Optimized the layout for different screen sizes
- Created hover and focus states for interactive elements

## Desktop view:
![Desktop view](design/desktop-design.jpg) 

## Mobile view:
![Mobile view](design/mobile-design.jpg) 